# Pokecommerce

## Description
This project was built for the final exercise on [ReactJS](https://www.coderhouse.com/online/reactjs) of the Coderhouse Platform.

## Technologies
To modify this project it is necessary to know the basic web technologies `html`, `css` and `javascript`. In addition to this, the application was built using the following technologies:

* [React](https://reactjs.org/) 
* [Firebase](https://firebase.google.com/)
* [React-Router](https://reactrouter.com/docs/en/v6) 
* [React-Hot-Toast](https://react-hot-toast.com/)
* [SCSS](https://sass-lang.com/)
* [React-Icons](https://react-icons.github.io/react-icons/)



## Installation

To install the project, you must have [NodeJS](https://nodejs.org/en/) installed on your computer. Once you have it, you must clone the repository and install the dependencies with the following command: 

#-. npm install and then run the project with npm start.
